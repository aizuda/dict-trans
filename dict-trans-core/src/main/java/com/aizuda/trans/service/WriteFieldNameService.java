package com.aizuda.trans.service;

import com.aizuda.trans.annotation.Translate;
import com.aizuda.trans.enums.FormatType;

import java.util.List;

/**
 * 写入字段名服务
 *
 * @author nn200433
 * @date 2024-01-29 01:50:54
 */
public interface WriteFieldNameService {

    /**
     * 获取领域名称列表
     *
     * @param translateConfig 翻译配置
     * @param originFieldName 原点字段名称
     * @param fieldFormatType 字段格式类型
     * @return {@link List }<{@link String }>
     * @author nn200433
     */
    public List<String> getFieldNameList(Translate translateConfig, String originFieldName, FormatType fieldFormatType);

}
