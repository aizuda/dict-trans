package com.aizuda.trans.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * 注解 构造器
 *
 * @author nn200433
 * @date 2024-01-29 01:41:01
 */
public class AnnotationBuilder {

    private final Class<? extends Annotation> annotationType;
    private final Map<String, Object>         values = new HashMap<String, Object>();

    public AnnotationBuilder(Class<? extends Annotation> annotationType) {
        this.annotationType = annotationType;
    }

    /**
     * 设值
     *
     * @param methodName 方法名称
     * @param value      价值
     * @author nn200433
     */
    public void setValue(String methodName, Object value) {
        values.put(methodName, value);
    }

    /**
     * 构建
     *
     * @return {@link Annotation }
     * @author nn200433
     */
    public Annotation build() {
        return (Annotation) Proxy.newProxyInstance(
                annotationType.getClassLoader(),
                new Class[]{annotationType},
                new AnnotationProxyBuilder(values)
        );
    }

}
