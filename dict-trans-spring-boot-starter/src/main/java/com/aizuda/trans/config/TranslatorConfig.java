package com.aizuda.trans.config;

import com.aizuda.trans.aspect.TranslateAspect;
import com.aizuda.trans.service.DictTranslateService;
import com.aizuda.trans.service.SummaryExtractService;
import com.aizuda.trans.service.WriteFieldNameService;
import com.aizuda.trans.service.impl.DataBaseTranslator;
import com.aizuda.trans.service.impl.DefaultDictTranslateServiceImpl;
import com.aizuda.trans.service.impl.DefaultSummaryExtractServiceImpl;
import com.aizuda.trans.service.impl.DesensitizedTranslator;
import com.aizuda.trans.service.impl.DictCacheTranslator;
import com.aizuda.trans.service.impl.EnumTranslator;
import com.aizuda.trans.service.impl.JsonConvertTranslator;
import com.aizuda.trans.service.impl.SummaryExtractTranslator;
import com.aizuda.trans.service.impl.defaults.DefaultWriteFieldNameServiceImpl;
import com.aizuda.trans.service.impl.wrapper.IPageUnWrapper;
import com.aizuda.trans.util.TranslatorUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.support.GenericConversionService;

import javax.sql.DataSource;

/**
 * 翻译配置
 *
 * @author nn200433
 * @date 2022-08-18 018 16:37:42
 */
@Configuration
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class TranslatorConfig {

    /**
     * 字典翻译切面
     *
     * @param conversionService 通用转换服务
     * @return {@link TranslateAspect }
     * @author nn200433
     */
    @Bean
    public TranslateAspect translateAspect(GenericConversionService conversionService) {
        return new TranslateAspect(conversionService);
    }

    /**
     * 注册字典翻译服务默认实现
     *
     * @return {@link DictTranslateService }
     * @author nn200433
     */
    @Bean
    @ConditionalOnMissingBean
    public DictTranslateService dictTranslateService() {
        return new DefaultDictTranslateServiceImpl();
    }

    /**
     * 注册摘要提取服务默认实现
     *
     * @return {@link DictTranslateService }
     * @author nn200433
     */
    @Bean
    @ConditionalOnMissingBean
    public SummaryExtractService summaryExtractService() {
        return new DefaultSummaryExtractServiceImpl();
    }

    /**
     * 注册写入字段服务默认实现
     *
     * @return {@link WriteFieldNameService }
     * @author nn200433
     */
    @Bean
    @ConditionalOnMissingBean
    public WriteFieldNameService writeFieldNameService() {
        return new DefaultWriteFieldNameServiceImpl();
    }

    /**
     * 注册IPage解包器
     *
     * @return {@link IPageUnWrapper }<{@link Object }>
     * @author nn200433
     */
    @Bean
    public IPageUnWrapper<Object> iPageUnWrapper() {
        return new IPageUnWrapper<Object>();
    }

    // region =========================================== 翻译服务 ===========================================

    /**
     * 数据库翻译默认实现
     *
     * @param dataSource 数据源
     * @return {@link DataBaseTranslator }
     * @author nn200433
     */
    @Bean
    @ConditionalOnMissingBean
    public DataBaseTranslator dataBaseTranslator(DataSource dataSource) {
        return new DataBaseTranslator(dataSource);
    }

    /**
     * 脱敏翻译
     *
     * @return {@link DesensitizedTranslator }
     * @author nn200433
     */
    @Bean
    @ConditionalOnMissingBean
    public DesensitizedTranslator desensitizedTranslator() {
        return new DesensitizedTranslator();
    }

    /**
     * 数据字典翻译
     *
     * @param dictTranslateService 字典翻译服务
     * @return {@link DictCacheTranslator }
     * @author nn200433
     */
    @Bean
    @ConditionalOnMissingBean
    public DictCacheTranslator dictCacheTranslator(DictTranslateService dictTranslateService) {
        return new DictCacheTranslator(dictTranslateService);
    }

    /**
     * 枚举翻译
     *
     * @return {@link EnumTranslator }
     * @author nn200433
     */
    @Bean
    @ConditionalOnMissingBean
    public EnumTranslator enumTranslator() {
        return new EnumTranslator();
    }

    /**
     * JSON 翻译转换器
     *
     * @return {@link JsonConvertTranslator }
     * @author nn200433
     */
    @Bean
    @ConditionalOnMissingBean
    public JsonConvertTranslator jsonConvertTranslator() {
        return new JsonConvertTranslator();
    }

    /**
     * 摘要提取翻译
     *
     * @param summaryExtractService 摘要提取服务
     * @return {@link SummaryExtractTranslator }
     * @author nn200433
     */
    @Bean
    @ConditionalOnMissingBean
    public SummaryExtractTranslator summaryExtractTranslator(SummaryExtractService summaryExtractService) {
        return new SummaryExtractTranslator(summaryExtractService);
    }

    // endregion ===============================================================================================

    /**
     * 翻译工具类
     *
     * @param conversionService 通用转换服务
     * @return {@link TranslatorUtil }
     * @author nn200433
     */
    @Bean
    public TranslatorUtil translatorUtil(GenericConversionService conversionService) {
        return new TranslatorUtil(conversionService);
    }

}
