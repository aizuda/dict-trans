package com.demo.trans.demo;

import com.demo.trans.entity.Device;
import com.demo.trans.entity.People;
import com.demo.trans.entity.People2;
import com.demo.trans.entity.People2_1;
import com.demo.trans.entity.People3;
import com.demo.trans.entity.People4;
import com.demo.trans.entity.Result;

import java.util.List;

/**
 * 演示服务
 *
 * @author nn200433
 * @date 2022-12-16 016 11:45:46
 */
public interface DemoService {
    
    /**
     * 字典演示
     *
     * @return {@link List }<{@link People }>
     * @author nn200433
     */
    public List<People> dictDemo();

    /**
     * 字典 & 脱敏 & 摘要提取 演示
     *
     * @return {@link List }<{@link People }>
     * @author nn200433
     */
    public List<People> dictDemo2();
    
    /**
     * 枚举演示
     *
     * @return {@link List }<{@link Device }>
     * @author nn200433
     */
    public List<Device> enumDemo();
    
    /**
     * 数据库演示
     *
     * @return {@link List }<{@link People2 }>
     * @author nn200433
     */
    public List<People2> dbDemo();

    /**
     * 数据库演示（单列翻译）
     *
     * @return {@link List }<{@link People2 }>
     * @author nn200433
     */
    public List<People2_1> dbDemo1();

    /**
     * json演示
     *
     * @return {@link List }<{@link People3 }>
     * @author nn200433
     */
    public List<People3> jsonDemo();

    /**
     * 响应嵌套模拟
     *
     * @return {@link List }<{@link People }>
     * @author nn200433
     */
    public List<People> responseNestedMock();

    /**
     * 响应嵌套模拟
     *
     * @return {@link Result }
     * @author nn200433
     */
    public Result responseNestedMockTest();

    /**
     * 20230822 示例
     *
     * @return {@link List }<{@link People4 }>
     * @author nn200433
     */
    public List<People4> demo0822();

    /**
     * demo0421
     *
     * @return {@link List }<{@link People }>
     * @author nn200433
     */
    public List<People> demo0421();

}
