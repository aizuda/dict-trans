package com.demo.trans.demo.impl;

import cn.hutool.core.lang.Validator;
import com.aizuda.trans.annotation.Dictionary;
import com.aizuda.trans.entity.ExtendParam;
import com.aizuda.trans.service.Translatable;
import com.demo.trans.enums.IdNumber;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * 身份证号码翻译
 *
 * @author nn200433
 * @date 2024-04-21 02:13:27
 */
@Component
public class IdNumberTranslatable implements Translatable {

    @Override
    public List<Object> translate(String origin, Dictionary dictConfig, ExtendParam extendParam) {
        final boolean            isIdNumber = Validator.isCitizenId(origin);
        final Optional<IdNumber> idNumber   = IdNumber.of(extendParam.getConditionValue());
        if (isIdNumber && idNumber.isPresent()) {
            return idNumber.get().call(origin);
        }
        return Collections.emptyList();
    }

}
