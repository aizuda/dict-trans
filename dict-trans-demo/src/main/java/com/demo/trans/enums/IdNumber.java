package com.demo.trans.enums;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * 身份证号码信息获取枚举类
 *
 * @author nn200433
 * @date 2024-04-21 02:32:23
 */
@Getter
public enum IdNumber {

    /** 年龄 */
    AGE("age", o -> Collections.singletonList(Convert.toStr(IdcardUtil.getAgeByIdCard(o)))),
    /** 省份 */
    PROVINCE("province", o -> Collections.singletonList(IdcardUtil.getProvinceByIdCard(o))),
    /** 出生日期 */
    BIRTH("birth", o -> Collections.singletonList(IdcardUtil.getBirth(o))),
    /** 性别 */
    SEX("sex", o -> Collections.singletonList(1 == IdcardUtil.getGenderByIdCard(o) ? "男" : "女"));

    private String                         condition;
    private Function<String, List<Object>> invoke;

    private IdNumber(String condition, Function<String, List<Object>> invoke) {
        this.condition = condition;
        this.invoke    = invoke;
    }

    /**
     * 根据 condition 获取枚举类
     *
     * @param condition 条件
     * @return {@link Optional }<{@link IdNumber }>
     * @author nn200433
     */
    public static Optional<IdNumber> of(String condition) {
        if (StrUtil.isBlank(condition)) {
            return Optional.empty();
        }
        return Arrays.stream(IdNumber.values()).filter(o -> condition.equals(o.getCondition())).findFirst();
    }

    /**
     * 调用
     *
     * @param idNumber 身份证号码
     * @return {@link List }<{@link String }>
     * @author nn200433
     */
    public List<Object> call(String idNumber) {
        return getInvoke().apply(idNumber);
    }

}
